FROM docker.io/tomcat:9-jre11-slim

# Draw.io image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    DRAW_VERSION=20.2.3

LABEL io.k8s.description="Draw.io is a Java based diagram solution." \
      io.k8s.display-name="Draw.io $DRAW_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="diagram,draw,drawio" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-draw" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$DRAW_VERSION"

COPY config/* /

RUN set -x \
    && echo "# Updates Stretch-Backports Keyrings" \
    && rm -f /etc/apt/sources.list.d/stretch-backports.list \
    && apt-get update \
    && apt-get install -y debian-archive-keyring debian-keyring \
    && echo "# Install Dumb-init" \
    && echo deb http://deb.debian.org/debian stretch-backports main \
	>/etc/apt/sources.list.d/stretch-backports.list \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Draw.io Dependencies" \
    && apt-get install -y --no-install-recommends openjdk-11-jdk-headless ant \
	git patch wget xmlstarlet curl libnss-wrapper \
    && echo "# Install Draw.io" \
    && ( \
	cd /tmp \
	&& wget https://github.com/jgraph/drawio/archive/v$DRAW_VERSION.zip \
	&& unzip v$DRAW_VERSION.zip \
	&& cd /tmp/drawio-$DRAW_VERSION \
	&& cd /tmp/drawio-$DRAW_VERSION/etc/build \
	&& ant war \
	&& cd /tmp/drawio-$DRAW_VERSION/build \
	&& unzip /tmp/drawio-$DRAW_VERSION/build/draw.war \
	    -d $CATALINA_HOME/webapps/draw; \
    ) \
    && echo "# Configure Draw.io" \
    && ( \
	cd $CATALINA_HOME \
	&& xmlstarlet ed \
	    -P -S -L \
	    -i '/Server/Service/Engine/Host/Valve' -t 'elem' -n 'Context' \
	    -i '/Server/Service/Engine/Host/Context' -t 'attr' -n 'path' -v '/' \
	    -i '/Server/Service/Engine/Host/Context[@path="/"]' -t 'attr' -n 'docBase' -v 'draw' \
	    -s '/Server/Service/Engine/Host/Context[@path="/"]' -t 'elem' -n 'WatchedResource' -v 'WEB-INF/web.xml' \
	    -i '/Server/Service/Engine/Host/Valve' -t 'elem' -n 'Context' \
	    -i '/Server/Service/Engine/Host/Context[not(@path="/")]' -t 'attr' -n 'path' -v '/ROOT' \
	    -s '/Server/Service/Engine/Host/Context[@path="/ROOT"]' -t 'attr' -n 'docBase' -v 'ROOT' \
	    -s '/Server/Service/Engine/Host/Context[@path="/ROOT"]' -t 'elem' -n 'WatchedResource' -v 'WEB-INF/web.xml' \
	    conf/server.xml; \
    ) \
    && echo "# Fixing permissions" \
    && mkdir -p /usr/local/tomcat/conf/Catalina/localhost \
    && chown -R 1001:root /usr/local/tomcat/conf/Catalina/localhost \
    && chmod -R g=u /usr/local/tomcat/conf/Catalina/localhost \
    && echo "# Cleaning Up" \
    && apt-get remove -y --purge openjdk-11-jdk-headless ant git patch wget \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/v$DRAW_VERSION.zip /root/.gnupg \
	/tmp/drawio-$DRAW_VERSION \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR $CATALINA_HOME
ENTRYPOINT ["dumb-init","--","/run-draw.sh"]
CMD "catalina.sh" "run"
