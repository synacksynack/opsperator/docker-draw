#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if test "`id -u`" -ne 0; then
    echo Setting up nsswrapper mapping `id -u` to drawio
    if grep ^drawio: /etc/passwd >/dev/null; then
	sed "s|^drawio:.*|drawio:x:`id -g`:|" /etc/group >/tmp/drawio-group
	sed \
	    "s|^drawio:.*|drawio:x:`id -u`:`id -g`:drawio:/var/www:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/drawio-passwd
    else
	(
	    cat /etc/passwd
	    echo "drawio:x:`id -u`:`id -g`:drawio:/var/www:/usr/sbin/nologin"
	) >/tmp/drawio-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/drawio-passwd
    export NSS_WRAPPER_GROUP=/tmp/drawio-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

exec "$@"
